const { parseString } = require('xml2js');
const { RuntimeError } = require('./errorClasses');
const { printData, printXml } = require('./utils');
const libraryXml = `<?xml version="1.0" encoding="UTF-8"?>
<library>
  <book>
    <title>Harry Potter and the Sorcerer's Stone</title>
    <author>J.K. Rowling</author>
    <publicationYear>1997</publicationYear>
  </book>
  <book>
    <title>The Hobbit</title>
    <author>J.R.R. Tolkien</author>
    <publicationYear>1937</publicationYear>
  </book>
  </library>
`;

readXml();
updateXml();

async function readXml() {
    try {
        const parsdData = await parseXml(libraryXml);
        const bookArray = parsdData?.library?.book;
        console.log('**Output of Question No: 1**')
        printData(bookArray);
      } catch (error) {
        throw new RuntimeError(`Runtime error occurred: ${error}`);
      }
}

async function updateXml() {
    try {
        const parsdData = await parseXml(libraryXml);
        const bookArray = parsdData?.library?.book;
        for(const book of bookArray) {
            book.title = ['Test'];
        }
        console.log('**Output of Question No: 5**')
        printXml({library: {book: bookArray}});
      } catch (error) {
        throw new RuntimeError(`Runtime error occurred: ${error}`);
      }
}

function parseXml(xmlData) {
    return new Promise((resolve, reject) => {
        parseString(xmlData, (err, result) => {
        if (err) {
            reject(err);
        } else {
            resolve(result);
        }
        });
    });
}
