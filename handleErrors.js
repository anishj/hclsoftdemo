const { ValidationError, RuntimeError, CustomError, Logger } = require('./errorClasses');
const { writeFile } = require('./utils');

try {
//checkValidationError(); // uncomment this to check validation error
checkRunTimeError(); // uncomment this to check runtime error
//checkCustomError(); // uncomment this to check custom error
} catch(e) {
    handleErrors(e);
}

function handleErrors(error) {
  console.log('**Output of Question No: 9 & 10**')
  const logMessage = `${error.name}: ${error.message}`;
  Logger.error(`${new Date().toISOString()} - ${logMessage}\n`)
}
function checkValidationError() {
    let userName;
    if(!userName) throw new ValidationError(`Invalid User Name`);
}
function checkRunTimeError() {
    try {
        let user;
        console.log(user.name)
      } catch (error) {
        throw new RuntimeError(`Runtime error occurred: ${error}`);
      }
}

function checkCustomError() {
    const age = 59;
    if(age < 60) throw new CustomError(`Not a senior citizion`);
}
