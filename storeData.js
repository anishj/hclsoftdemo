const fs = require('fs');
const crypto = require('crypto');
require('dotenv').config();
const secretKey = process.env.SECRET_KEY;
const { Readable } = require('stream');
const dataToStore = [1,2,3,4,5];
storeData('saved_data.txt', dataToStore);

function storeData(filename, data) {
  const key = crypto.createHash('sha256').update(secretKey).digest();
  const iv = crypto.randomBytes(16);
  const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);

  // Since the data is a large amount, as mentioned in your question, using streams for reading and writing.
  const dataStream = Readable.from([JSON.stringify(data)]);
  const writeStream = fs.createWriteStream(filename, 'utf8');
  writeStream.write(iv.toString('hex'), 'hex');
  dataStream.pipe(cipher).pipe(writeStream);
  writeStream.on('finish', () => {
    console.log('**Output of Question No: 7**')
    console.log('Data is securely stored in the file saved_data.txt.');
  });

  writeStream.on('error', (err) => {
    console.error('Error storing data:', err);
  });
}

