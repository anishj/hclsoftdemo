const { Builder } = require('xml2js');
function printData(bookArray) {
    const titles = bookArray.map(book => Array.isArray(book.title) ? book.title[0] : book.title);
    const authors = bookArray.map(book => Array.isArray(book.author) ? book.author[0] : book.author);
    const publicationYears = bookArray.map(book => Array.isArray(book.publicationYear) ? book.publicationYear[0] : book.publicationYear);
    console.log(`title-> ${titles.join(', ')}`);
    console.log(`author-> ${authors.join(', ')}`);
    console.log(`publication year-> ${publicationYears.join(', ')}`);
}

function printXml(jsonData) {
    const builder = new Builder();
    const updatedXml = builder.buildObject(jsonData);
    console.log(updatedXml);
}

module.exports = { printData, printXml }