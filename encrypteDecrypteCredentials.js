const crypto = require('crypto');
require('dotenv').config();
const secretKey = process.env.SECRET_KEY;

function encrypt(text) {
    const key = crypto.createHash('sha256').update(secretKey).digest();
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return { iv: iv.toString('hex'), encryptedData: encrypted };
  }
  
  function decrypt(encryptedObj) {
    const key = crypto.createHash('sha256').update(secretKey).digest();
    const decipher = crypto.createDecipheriv('aes-256-cbc', key, Buffer.from(encryptedObj.iv, 'hex'));
    let decrypted = decipher.update(encryptedObj.encryptedData, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
  }
  
  const username = 'test';
  const password = 'test@1234';
  console.log('**Output of Question No: 6**')
  const userNameObj = encrypt(username);
  console.log('Encrypted Username:', userNameObj.encryptedData);
  const passwordObj = encrypt(password);
  console.log('Encrypted Password:', passwordObj.encryptedData);
  
  const decryptedUsername = decrypt(userNameObj);
  console.log('Decrypted Username:', decryptedUsername);
  const decryptedPassword = decrypt(passwordObj);
  console.log('Decrypted Password:', decryptedPassword);