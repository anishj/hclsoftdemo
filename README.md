# Project Name

HCL software - Coding Test

## Installation

Instructions on how to install the project:

1. Clone the repository: `git clone https://gitlab.com/anishj/hclsoftdemo.git`
2. Install dependencies: `npm install`

## Usage

How to run the application:

1. For **Questions 1 & 5**, Run: `npm run xml`
2. For **Questions 2, 3 & 4**, Run: `npm run json`
3. For **Question 6**, Run: `npm run encryptdecrypt`
4. For **Question 7**, Run: `npm run storedata`
5. For **Question 8**, Run: `npm run clustering`
6. For **Questions 9 & 10**, Run: `npm run handleerrors`

## .env File

This repository includes a `.env` file for demonstration purposes. However, it is important to note that committing sensitive information such as API keys or passwords in the `.env` file is not recommended.


