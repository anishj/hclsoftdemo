const { printData } = require('./utils');
const jsonData = {
    "library": [
      {
        "title": "Harry Potter and the Sorcerer's Stone",
        "author": "J.K. Rowling",
        "publicationYear": 1997
      },
      {
        "title": "The Hobbit",
        "author": "J.R.R. Tolkien",
        "publicationYear": 1937
      }
    ]
  };
  
readJson();
updateJson();

function readJson() {
    console.log('**Output of Question No: 2**')
    printData(jsonData.library)
}

function updateJson() {
    console.log('**Output of Question No: 3 & 4**')
    jsonData.library.push({ title: 'Test', author: 'Test', publicationYear: 'Test'});
    printData(jsonData.library)
}
