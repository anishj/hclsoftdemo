const fs = require('fs').promises;
class ValidationError extends Error {
    constructor(message) {
      super(message);
      this.name = 'ValidationError';
    }
  }
  
class RuntimeError extends Error {
    constructor(message) {
      super(message);
      this.name = 'RuntimeError';
    }
}

class CustomError extends Error {
    constructor(message) {
      super(message);
      this.name = 'CustomError';
    }
}

class Logger {
    static async error(content) {
        await fs.appendFile('error.log', content);
        console.log(`${content}. Check error.log file for log details`)
    }
}

module.exports = { ValidationError, RuntimeError, CustomError, Logger }