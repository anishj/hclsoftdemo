const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
if (cluster.isMaster) {
 console.log('**Output of Question No: 8**')
  console.log(`Master process with id ${process.pid} started`);
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }
} else {
    startNewWorker()
}

function startNewWorker() {
    console.log(`Worker with id ${process.pid} started`);
    setTimeout(() => {
        console.log(`Task Completed. Worker ${process.pid}`);
        process.exit(0); // Exit after completing task
    }, 2000);
}
